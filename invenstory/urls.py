from . import views
from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('inventory/', include('inven.urls')),
    path('productlist/', include('productlist.urls')),
    path('myinventory/', include('myinventory.urls')),
    path('', views.landing, name='landing'),
    path('sold/', include('terjual.urls')),
    path('accounts/', include('accounts.urls')),
]

urlpatterns += staticfiles_urlpatterns()
