from django.contrib import admin
from django.urls import path
from . import views

app_name = 'terjual'

urlpatterns = [
    path('', views.sold, name = 'sold'),
    path('<int:id>/', views.sold, name='sold'),
    path('ajax_call/', views.ajax_call),
    path('return_makanan/', views.return_makanan)
]

