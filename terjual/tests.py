from django.test import TestCase, Client
from inven.models import BahanMakanan, Makanan
from .models import AddMakanan
from myinventory.models import NamaBarang
from django.contrib.auth.models import User
from django.test import Client

class UnitTestTerjual(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        mak = Makanan(makanan = 'kue jepang')
        mak.save()
        terj = AddMakanan(
            makanan = mak,
            jumlah_terjual = 0,
        )
        terj.save()
        ing = BahanMakanan(
            bahan = 'telur',
            jumlah = 1,
        )
        ing.save()
        ing.makanan.add(mak)
        juml = NamaBarang(
            namaproduk = mak,
            namabahan = ing,
            jumlah = 0
        )
        juml.save()

    def test_url_is_exist(self):
        self.orang = Client()
        Client().login(username='john', password='johnpassword')
        data = {'username':'john', 'password':'johnpassword'}
        response = self.orang.post('/accounts/login/', data)
        response = self.orang.get('/sold/')
        self.assertEqual(response.status_code, 200)
    
    
    def test_view(self):
        self.orang = Client()
        Client().login(username='john', password='johnpassword')
        data = {'username':'john', 'password':'johnpassword'}
        response = self.orang.post('/accounts/login/', data)
        data = {
            'makanan' : 1
        }
        response = self.orang.post('/sold/1/', data)
        self.assertEqual(response.status_code, 302)
