from django.test import TestCase, Client
from inven.models import BahanMakanan, Makanan
from myinventory.models import NamaBarang
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.test import Client

class UnitTestMyInventory(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        mak = Makanan(makanan='kue padang')
        mak.save()
        
        ing = BahanMakanan(bahan='telur', jumlah=20)
        ing.save()
        ing.makanan.add(mak)
        barang = NamaBarang(
            namaproduk = mak,
            namabahan = ing,
            jumlah = 1
        )
        barang.save()

    def test_url_is_exist(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/myinventory/')
        self.assertEqual(response.status_code, 200)

    def test_save_lewat_html(self):
        self.orang = Client()
        Client().login(username='john', password='johnpassword')
        data = {'username':'john', 'password':'johnpassword'}
        response = self.orang.post('/accounts/login/', data)
        data={
            'makanan' : 20
        }
        response = self.orang.post('/myinventory/1/', data )
        self.assertEqual(response.status_code, 302)
