from . import views
from django.urls import path

app_name = 'inventory'

urlpatterns = [
    path('add/', views.add, name='addProduct',), 
    path('add/json_call/<str:isi>/', views.ajax_call),
]