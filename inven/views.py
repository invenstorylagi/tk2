from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Makanan, BahanMakanan
from .forms import AddProduct, Formset, AddBahan
from myinventory.models import NamaBarang
from terjual.models import AddMakanan
from django.contrib.auth.decorators import login_required
import json

@login_required(login_url='/accounts/login/')
def add(request):
    if request.method == 'GET':
        formset = Formset(request.GET or None)
        form = AddProduct()
    elif request.method == "POST" :
        form = AddProduct(request.POST)
        formset = Formset(request.POST)
        if formset.is_valid() and form.is_valid():
            mm = form.cleaned_data['makanan']
            if (Makanan.objects.filter(makanan=mm).count() == 0):
                mak = Makanan(
                    makanan = form.cleaned_data['makanan']
                )
                mak.save()
                terj = AddMakanan(
                    makanan = mak,
                    jumlah_terjual = 0,
                )
                terj.save()
                for form in formset:
                    ing = BahanMakanan(
                        bahan = form.cleaned_data.get('bahan'),
                        jumlah = form.cleaned_data.get('jumlah'),
                    )
                    ing.save()
                    ing.makanan.add(mak)
                    juml = NamaBarang(
                        namaproduk = mak,
                        namabahan = ing,
                        jumlah = 0
                    )
                    juml.save()
            return redirect('inventory:addProduct')
    return render(request, 'inven/add.html', {'formset':formset, 'form':form })

def ajax_call(request, isi):
    data_search = Makanan.objects.filter(makanan__startswith=(isi))
    if len(data_search) == 0:
        result = ['Makanan is Available']
    else:
        result = ['Makanan is Registered!!']
    data = json.dumps(result)
    return HttpResponse(data, content_type="application/json")