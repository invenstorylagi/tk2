from django.test import TestCase, Client
from django.urls import resolve
from .models import BahanMakanan, Makanan
from .forms import Formset
from .views import *
from django.contrib.auth.models import User
from django.test import Client

class UnitTestAddProduct(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def test_url_is_exist(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/inventory/add/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Add Product", response.content.decode())
        self.assertNotContains(response, 'ahisfbdjksdn')

    def test_ada_object(self):
        mak = Makanan(makanan='kue padang')
        mak.save()
        ing = BahanMakanan(bahan='telur', jumlah=20)
        ing.save()
        ing.makanan.add(mak)
        self.assertEqual(ing.bahan, 'telur')
        self.assertEqual(ing.jumlah, 20)
        self.assertEqual(mak.makanan, 'kue padang')
    
    def test_save_lewat_html(self):
        self.orang = Client()
        Client().login(username='john', password='johnpassword')
        data = {'username':'john', 'password':'johnpassword'}
        response = self.orang.post('/accounts/login/', data)
        data={
            'form-TOTAL_FORMS': 1, 
            'form-INITIAL_FORMS': 1,
            'form-MIN_NUM_FORMS': ['0'], 
            'form-MAX_NUM_FORMS': ['1000'], 
            'form-0-bahan': ['kue medan'], 
            'form-0-jumlah': ['1'],
            'makanan' : 'kue medan',
            'bahan' : 'telur',
            'jumlah' : 20
        }
        response = self.orang.post('/inventory/add/', data )
        self.assertEqual(response.status_code, 302)
        response = self.orang.get('/inventory/add/json_call/kue medan/')
        self.assertEqual(response.content.decode(), '["Makanan is Registered!!"]')


    def test_add_function_used(self):
        found = resolve('/inventory/add/')
        self.assertEqual(found.func, add)

    def test_model_method_ambil(self):
        ha = Makanan.objects.create(makanan = 'ayam')
        ha.save()
        hya = BahanMakanan.objects.create(
            bahan = 'bawang',
            jumlah = 20,
        )
        hya.save()
        hya.makanan.add(ha)
        hya = BahanMakanan.objects.create(
            bahan = 'bombay',
            jumlah = 20,
        )
        hya.save()
        hya.makanan.add(ha)
        for i in Makanan.objects.all():
            makanan = i.makanan
            for j in i.ambil():
                bahan = j.bahan
                jumlah = j.jumlah
    def test_json(self):
        response = self.client.get('/inventory/add/json_call/hahaha/')
        self.assertEqual(response.content.decode(), '["Makanan is Available"]')
